import os
import shutil
import pandas as pd
import wmi
import time
import filedate
 



def format_file_size(size, decimals=2, binary_system=True):
    if binary_system:
        units = ['B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB']
        largest_unit = 'YiB'
        step = 1024
    else:
        units = ['B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB']
        largest_unit = 'YB'
        step = 1000

    for unit in units:
        if size < step:
            return ('%.' + str(decimals) + 'f %s') % (size, unit)
        size /= step

    return ('%.' + str(decimals) + 'f %s') % (size, largest_unit)


disco = "E:"


used = shutil.disk_usage(disco).used
c = wmi.WMI ()
for drive in c.Win32_LogicalDisk ():
  if drive.Caption == disco:
     disco_label = drive.VolumeName

print(f"Used: {used/(1000000000)} GB")
df_files = pd.DataFrame({'Nombre' : [], 'Ruta' : [], 'Tamaño' : [], 'Creado': [], 'Modificado': []})

print(disco_label)

leido = 0
porcentaje_previo = 0
for path, currentDirectory, files in os.walk(disco):
    for file in files:
        
        
        file_path = os.path.join(path,file)
        

        if os.path.isfile(file_path):
            leido += os.stat(file_path).st_size
            porcentaje = round(100.0 * (leido / used))
            
            file_ = file.encode("latin1", 'ignore').decode("latin1")
            file_path_ = file_path.encode("latin1", 'ignore').decode("latin1")

            if file_path_ != file_path:
                print(file_path_)
                print(file_path)

            size = format_file_size(os.stat(file_path).st_size)
            creado = time.ctime(os.path.getctime(file_path))
            modificado = time.ctime(os.path.getmtime(file_path))
            list_row = [file_, file_path_, size, creado, modificado]
            df_files.loc[len(df_files)] = list_row
            if porcentaje > porcentaje_previo:
                
                if porcentaje_previo == 0:
                    df_files.to_csv(disco_label+'.csv', index=False, encoding='latin1')
                    df_files = pd.DataFrame({'Nombre' : [], 'Ruta' : [], 'Tamaño' : [], 'Creado': [], 'Modificado': []})
                else:
                    df_files.to_csv(disco_label+'.csv', mode='a', index=False, header=False, encoding='latin1')

                porcentaje_previo = porcentaje
                print(leido,used,porcentaje)


# print("número de archivos:", count)