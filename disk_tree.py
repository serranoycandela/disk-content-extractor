#-*- encoding: utf-8 -*-

from PySide2 import QtCore, QtWidgets
from PySide2.QtWidgets import QFileSystemModel
import sys
import gui
import wmi
import shutil
from pathlib import Path
import filedate
import os
import pandas as pd
import time

class Ui_MainWindow(QtWidgets.QMainWindow, gui.Ui_MainWindow):

    def __init__(self, parent=None):
        super(Ui_MainWindow, self).__init__(parent)
        self.setupUi(self)
        c = wmi.WMI ()
        for drive in c.Win32_LogicalDisk ():
            usado = self.format_file_size(shutil.disk_usage(drive.Caption).used, binary_system=False)
            capacidad = self.format_file_size(shutil.disk_usage(drive.Caption).total, binary_system=False)

            self.lista_captura.addItem(drive.VolumeName+" ("+drive.Caption+") "+usado +" de "+ capacidad)

        self.fsModel = QFileSystemModel()
        self.fsModel.setFilter(QtCore.QDir.AllDirs | QtCore.QDir.NoDotAndDotDot)
        
        self.tree_guardar.setModel(self.fsModel)
        self.tree_guardar.setColumnWidth(0, 361)
        self.tree_guardar.setColumnHidden(1, True)
        self.tree_guardar.setColumnHidden(2, True)
        self.fsModel.setRootPath("")
        self.tree_guardar.setSortingEnabled(True)
        
        self.tree_guardar.setRootIndex(self.fsModel.index(""))

        self.progressBar.setEnabled(True)
        self.progressBar.hide()
        

        self.tree_guardar.selectionModel().selectionChanged.connect(self.carpetaPicada)

        self.lista_captura.itemClicked.connect(self.capturaClicked)
        self.actionLista_de_archivos.triggered.connect(self.exporta_lista)
        self.actionArbol_de_archivos.triggered.connect(self.exporta_arbol)
        self.arbol_button.clicked.connect(self.exporta_arbol)
        self.lista_button.clicked.connect(self.exporta_lista)
        self.actualiza_discos.clicked.connect(self.actualizar_discos)


    def actualizar_discos(self):
        self.lista_captura.clear()
        c = wmi.WMI ()
        for drive in c.Win32_LogicalDisk ():
            usado = self.format_file_size(shutil.disk_usage(drive.Caption).used, binary_system=False)
            capacidad = self.format_file_size(shutil.disk_usage(drive.Caption).total, binary_system=False)

            self.lista_captura.addItem(drive.VolumeName+" ("+drive.Caption+") "+usado +" de "+ capacidad)

    def carpetaPicada(self, selected, deselected):
        self.selected_guardar.setText(self.fsModel.filePath(selected[0].topLeft()))
        print(self.selected_captura.text())
        if self.selected_captura.text() != "Selecciona disco...":
            self.arbol_button.setEnabled(True)
            self.lista_button.setEnabled(True)
            self.actionArbol_de_archivos.setEnabled(True)
            self.actionLista_de_archivos.setEnabled(True)
        
    def capturaClicked(self,item):
        self.selected_captura.setText(item.text())
        if not self.selected_guardar.text() == "Selecciona disco...":
            self.arbol_button.setEnabled(True)
            self.lista_button.setEnabled(True)
            self.actionArbol_de_archivos.setEnabled(True)
            self.actionLista_de_archivos.setEnabled(True)
            
        

    def exporta_arbol(self):
        disco = self.selected_captura.text().split("(")[1].split(")")[0]
        emty_root = self.selected_guardar.text()
        c = wmi.WMI ()
        for drive in c.Win32_LogicalDisk ():
            if drive.Caption == disco:
                disco_label = drive.VolumeName

        self.status.setText("Exportando árbol de archvos de "+disco_label+" en "+emty_root)
        used = shutil.disk_usage(disco).used
        leido = 0
        porcentaje_previo = 0
        self.progressBar.show()
        for path, currentDirectory, files in os.walk(disco):
            for file in files:
                try:
                    file_path = os.path.join(path,file)
                    self.label.setText(file_path)
                    QtWidgets.QApplication.processEvents()
                    if os.path.isfile(file_path):
                        leido += os.stat(file_path).st_size
                        porcentaje = round(100.0 * (leido / used))
                        file_path_ = file_path.encode("latin1", 'ignore').decode("latin1")
                        
                        if not "RECYCLE.BIN" in file_path_: 
                            
                            empty_path_txt = path.replace(disco,"")
                            empty_path = Path(emty_root) / disco_label / Path(empty_path_txt)
                            empty_path.mkdir(parents=True, exist_ok=True)
                            empty_file = empty_path / Path(file)
                            with open(empty_file.resolve(), 'w') as fp:
                                pass

                            
                            a_file = filedate.File(file_path)
                            metadatos = a_file.get()
                            
                            e_file = filedate.File(empty_file)
                            e_file.set(created = metadatos["created"].strftime("%Y.%m.%d %H:%M:%S"), modified = metadatos["modified"].strftime("%Y.%m.%d %H:%M:%S"), accessed = metadatos["accessed"].strftime("%Y.%m.%d %H:%M:%S"))
                            
                            
                            if porcentaje > porcentaje_previo:
                                self.progressBar.setValue(porcentaje)
                                porcentaje_previo = porcentaje
                                print(leido,used,porcentaje)
                except:
                    print("algo falló con:")
                    if file_path_:
                        print(file_path_)
                    else:
                        print(file)

        print("árbol listo")
        self.progressBar.setValue(100)
        time.sleep(1)
        self.progressBar.hide()
        self.status.setText("Árbol de archvos de "+disco_label+" exportado en "+emty_root)
        self.label.setText("Árbol exportado correctamente")


    def exporta_lista(self):
        disco = self.selected_captura.text().split("(")[1].split(")")[0]
        emty_root = self.selected_guardar.text()
        used = shutil.disk_usage(disco).used
        c = wmi.WMI ()
        for drive in c.Win32_LogicalDisk ():
            if drive.Caption == disco:
                disco_label = drive.VolumeName
        df_files = pd.DataFrame({'Nombre' : [], 'Ruta' : [], 'Tamaño' : [], 'Creado': [], 'Modificado': []})
        self.status.setText("Exportando lista de archvos de "+disco_label+" en "+emty_root)
        print(disco_label)
        csv_path = os.path.join(emty_root, disco_label+'.csv')
        self.progressBar.show()
        leido = 0
        porcentaje_previo = 0
        for path, currentDirectory, files in os.walk(disco):
            for file in files:
                try:
                
                    file_path = os.path.join(path,file)
                    self.label.setText(file_path)
                    QtWidgets.QApplication.processEvents()
                    if os.path.isfile(file_path):
                        file_path_ = file_path.encode("latin1", 'ignore').decode("latin1")
                        if not "RECYCLE.BIN" in file_path_: 
                            leido += os.stat(file_path).st_size
                            porcentaje = round(100.0 * (leido / used))
                            
                            file_ = file.encode("latin1", 'ignore').decode("latin1")
                            

                            if file_path_ != file_path:
                                print(file_path_)
                                print(file_path)

                            size = self.format_file_size(os.stat(file_path).st_size)
                            creado = time.ctime(os.path.getctime(file_path))
                            modificado = time.ctime(os.path.getmtime(file_path))
                            list_row = [file_, file_path_, size, creado, modificado]
                            df_files.loc[len(df_files)] = list_row
                            if porcentaje > porcentaje_previo:
                                self.progressBar.setValue(porcentaje)
                                if porcentaje_previo == 0:
                                    df_files.to_csv(csv_path, index=False, encoding='latin1')
                                    df_files = pd.DataFrame({'Nombre' : [], 'Ruta' : [], 'Tamaño' : [], 'Creado': [], 'Modificado': []})
                                else:
                                    df_files.to_csv(csv_path, mode='a', index=False, header=False, encoding='latin1')

                                porcentaje_previo = porcentaje
                                print(leido,used,porcentaje)

                except:
                    print("algo falló con:")
                    if file_path_:
                        print(file_path_)
                    else:
                        print(file)
            
        print("lista lista")
        self.progressBar.setValue(100)
        time.sleep(1)
        self.progressBar.hide()
        self.status.setText("Lista de archvos de "+disco_label+" exportada en "+emty_root)
        self.label.setText("Lista exportada correctamente")

    def format_file_size(self, size, decimals=2, binary_system=True):
        if binary_system:
            units = ['B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB']
            largest_unit = 'YiB'
            step = 1024
        else:
            units = ['B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB']
            largest_unit = 'YB'
            step = 1000

        for unit in units:
            if size < step:
                return ('%.' + str(decimals) + 'f %s') % (size, unit)
            size /= step

        return ('%.' + str(decimals) + 'f %s') % (size, largest_unit)

app = QtWidgets.QApplication([])
# filter = WheelEventFilter()
# app.installEventFilter(filter)
# app.setStyleSheet("QMessageBox { messagebox-text-interaction-flags: 5; }")
form = Ui_MainWindow()
form.show()


app.exec_()