import os
import shutil
from pathlib import Path
import wmi
import time
import filedate



disco = "F:"

used = shutil.disk_usage(disco).used
total = shutil.disk_usage(disco).total



c = wmi.WMI ()
for drive in c.Win32_LogicalDisk ():
  if drive.Caption == disco:
     disco_label = drive.VolumeName

emty_root = "C:/Users/serra/GitLab/disk-content-extractor"

def format_file_size(size, decimals=2, binary_system=True):
    if binary_system:
        units = ['B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB']
        largest_unit = 'YiB'
        step = 1024
    else:
        units = ['B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB']
        largest_unit = 'YB'
        step = 1000

    for unit in units:
        if size < step:
            return ('%.' + str(decimals) + 'f %s') % (size, unit)
        size /= step

    return ('%.' + str(decimals) + 'f %s') % (size, largest_unit)

c = wmi.WMI ()
for drive in c.Win32_LogicalDisk ():
  print(drive.Caption, drive.VolumeName, format_file_size(shutil.disk_usage(drive.Caption).used), format_file_size(shutil.disk_usage(drive.Caption).total))


print(f"Used: {used/(1000000000)} GB")


leido = 0
porcentaje_previo = 0
for path, currentDirectory, files in os.walk(disco):
    for file in files:
        file_path = os.path.join(path,file)
        if os.path.isfile(file_path):
            file_path_ = file_path.encode("latin1", 'ignore').decode("latin1")
            if not "RECYCLE.BIN" in file_path_:
                leido += os.stat(file_path).st_size
                porcentaje = round(100.0 * (leido / used))
                
                empty_path_txt = path.replace(disco,"")
                empty_path = Path(emty_root) / disco_label / Path(empty_path_txt)
                empty_path.mkdir(parents=True, exist_ok=True)
                empty_file = empty_path / Path(file)
                with open(empty_file.resolve(), 'w') as fp:
                    pass

                a_file = filedate.File(file_path)
                metadatos = a_file.get()
                
                e_file = filedate.File(empty_file)
                e_file.set(created = metadatos["created"].strftime("%Y.%m.%d %H:%M:%S"), modified = metadatos["modified"].strftime("%Y.%m.%d %H:%M:%S"), accessed = metadatos["accessed"].strftime("%Y.%m.%d %H:%M:%S"))
                
                
                if porcentaje > porcentaje_previo:
                    porcentaje_previo = porcentaje
                    print(leido,used,porcentaje)






