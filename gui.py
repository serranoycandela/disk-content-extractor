# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'gui.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.setEnabled(True)
        MainWindow.resize(832, 594)
        sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
        MainWindow.setSizePolicy(sizePolicy)
        MainWindow.setToolTipDuration(3)
        self.actionLista_de_archivos = QAction(MainWindow)
        self.actionLista_de_archivos.setObjectName(u"actionLista_de_archivos")
        self.actionLista_de_archivos.setEnabled(False)
        self.actionArbol_de_archivos = QAction(MainWindow)
        self.actionArbol_de_archivos.setObjectName(u"actionArbol_de_archivos")
        self.actionArbol_de_archivos.setEnabled(False)
        self.centralWidget = QWidget(MainWindow)
        self.centralWidget.setObjectName(u"centralWidget")
        self.verticalLayout_2 = QVBoxLayout(self.centralWidget)
        self.verticalLayout_2.setSpacing(6)
        self.verticalLayout_2.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.horizontalLayout_6 = QHBoxLayout()
        self.horizontalLayout_6.setSpacing(6)
        self.horizontalLayout_6.setObjectName(u"horizontalLayout_6")
        self.horizontalLayout_5 = QHBoxLayout()
        self.horizontalLayout_5.setSpacing(6)
        self.horizontalLayout_5.setObjectName(u"horizontalLayout_5")
        self.verticalLayout_3 = QVBoxLayout()
        self.verticalLayout_3.setSpacing(6)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setSpacing(6)
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.horizontalLayout_3.setContentsMargins(11, -1, -1, -1)
        self.label_2 = QLabel(self.centralWidget)
        self.label_2.setObjectName(u"label_2")

        self.horizontalLayout_3.addWidget(self.label_2)

        self.selected_captura = QLabel(self.centralWidget)
        self.selected_captura.setObjectName(u"selected_captura")

        self.horizontalLayout_3.addWidget(self.selected_captura)

        self.actualiza_discos = QPushButton(self.centralWidget)
        self.actualiza_discos.setObjectName(u"actualiza_discos")
        self.actualiza_discos.setMaximumSize(QSize(30, 16777215))

        self.horizontalLayout_3.addWidget(self.actualiza_discos)


        self.verticalLayout_3.addLayout(self.horizontalLayout_3)

        self.lista_captura = QListWidget(self.centralWidget)
        self.lista_captura.setObjectName(u"lista_captura")

        self.verticalLayout_3.addWidget(self.lista_captura)


        self.horizontalLayout_5.addLayout(self.verticalLayout_3)

        self.verticalLayout_4 = QVBoxLayout()
        self.verticalLayout_4.setSpacing(6)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.horizontalLayout_4 = QHBoxLayout()
        self.horizontalLayout_4.setSpacing(6)
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.horizontalLayout_4.setContentsMargins(11, -1, -1, -1)
        self.label_3 = QLabel(self.centralWidget)
        self.label_3.setObjectName(u"label_3")

        self.horizontalLayout_4.addWidget(self.label_3)

        self.selected_guardar = QLabel(self.centralWidget)
        self.selected_guardar.setObjectName(u"selected_guardar")

        self.horizontalLayout_4.addWidget(self.selected_guardar)

        self.actualiza_destinos = QPushButton(self.centralWidget)
        self.actualiza_destinos.setObjectName(u"actualiza_destinos")
        self.actualiza_destinos.setMaximumSize(QSize(30, 16777215))

        self.horizontalLayout_4.addWidget(self.actualiza_destinos)


        self.verticalLayout_4.addLayout(self.horizontalLayout_4)

        self.tree_guardar = QTreeView(self.centralWidget)
        self.tree_guardar.setObjectName(u"tree_guardar")

        self.verticalLayout_4.addWidget(self.tree_guardar)


        self.horizontalLayout_5.addLayout(self.verticalLayout_4)

        self.horizontalLayout_5.setStretch(0, 1)
        self.horizontalLayout_5.setStretch(1, 1)

        self.horizontalLayout_6.addLayout(self.horizontalLayout_5)

        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.setSpacing(6)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.arbol_button = QPushButton(self.centralWidget)
        self.arbol_button.setObjectName(u"arbol_button")
        self.arbol_button.setEnabled(False)
        self.arbol_button.setMinimumSize(QSize(140, 140))
        font = QFont()
        font.setPointSize(12)
        self.arbol_button.setFont(font)
        self.arbol_button.setToolTipDuration(3)

        self.verticalLayout.addWidget(self.arbol_button)

        self.lista_button = QPushButton(self.centralWidget)
        self.lista_button.setObjectName(u"lista_button")
        self.lista_button.setEnabled(False)
        self.lista_button.setMinimumSize(QSize(140, 140))
        self.lista_button.setFont(font)

        self.verticalLayout.addWidget(self.lista_button)


        self.horizontalLayout_6.addLayout(self.verticalLayout)


        self.verticalLayout_2.addLayout(self.horizontalLayout_6)

        self.status = QLabel(self.centralWidget)
        self.status.setObjectName(u"status")
        font1 = QFont()
        font1.setPointSize(11)
        self.status.setFont(font1)

        self.verticalLayout_2.addWidget(self.status)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setSpacing(6)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalLayout_2.setContentsMargins(-1, -1, 50, -1)
        self.progressBar = QProgressBar(self.centralWidget)
        self.progressBar.setObjectName(u"progressBar")
        self.progressBar.setEnabled(False)
        self.progressBar.setValue(0)

        self.horizontalLayout_2.addWidget(self.progressBar)


        self.verticalLayout_2.addLayout(self.horizontalLayout_2)

        self.scrollArea = QScrollArea(self.centralWidget)
        self.scrollArea.setObjectName(u"scrollArea")
        sizePolicy1 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.scrollArea.sizePolicy().hasHeightForWidth())
        self.scrollArea.setSizePolicy(sizePolicy1)
        self.scrollArea.setMaximumSize(QSize(16777215, 30))
        self.scrollArea.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.scrollArea.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.scrollArea.setWidgetResizable(True)
        self.scrollAreaWidgetContents = QWidget()
        self.scrollAreaWidgetContents.setObjectName(u"scrollAreaWidgetContents")
        self.scrollAreaWidgetContents.setGeometry(QRect(0, -3, 812, 31))
        self.horizontalLayout = QHBoxLayout(self.scrollAreaWidgetContents)
        self.horizontalLayout.setSpacing(6)
        self.horizontalLayout.setContentsMargins(11, 11, 11, 11)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.label = QLabel(self.scrollAreaWidgetContents)
        self.label.setObjectName(u"label")
        sizePolicy2 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy2)

        self.horizontalLayout.addWidget(self.label)

        self.scrollArea.setWidget(self.scrollAreaWidgetContents)

        self.verticalLayout_2.addWidget(self.scrollArea)

        MainWindow.setCentralWidget(self.centralWidget)
        self.menuBar = QMenuBar(MainWindow)
        self.menuBar.setObjectName(u"menuBar")
        self.menuBar.setGeometry(QRect(0, 0, 832, 21))
        self.menuExtraer = QMenu(self.menuBar)
        self.menuExtraer.setObjectName(u"menuExtraer")
        MainWindow.setMenuBar(self.menuBar)
        self.mainToolBar = QToolBar(MainWindow)
        self.mainToolBar.setObjectName(u"mainToolBar")
        MainWindow.addToolBar(Qt.TopToolBarArea, self.mainToolBar)
        self.statusBar = QStatusBar(MainWindow)
        self.statusBar.setObjectName(u"statusBar")
        MainWindow.setStatusBar(self.statusBar)

        self.menuBar.addAction(self.menuExtraer.menuAction())
        self.menuExtraer.addAction(self.actionLista_de_archivos)
        self.menuExtraer.addAction(self.actionArbol_de_archivos)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"DiskTree", None))
        self.actionLista_de_archivos.setText(QCoreApplication.translate("MainWindow", u"Lista de archivos", None))
        self.actionArbol_de_archivos.setText(QCoreApplication.translate("MainWindow", u"Arbol de archivos", None))
        self.label_2.setText(QCoreApplication.translate("MainWindow", u"Disco a capturar:", None))
        self.selected_captura.setText(QCoreApplication.translate("MainWindow", u"Selecciona disco...", None))
#if QT_CONFIG(tooltip)
        self.actualiza_discos.setToolTip("")
#endif // QT_CONFIG(tooltip)
        self.actualiza_discos.setText("")
        self.label_3.setText(QCoreApplication.translate("MainWindow", u"Disco donde guardar:", None))
        self.selected_guardar.setText(QCoreApplication.translate("MainWindow", u"Selecciona disco...", None))
#if QT_CONFIG(tooltip)
        self.actualiza_destinos.setToolTip("")
#endif // QT_CONFIG(tooltip)
        self.actualiza_destinos.setText("")
#if QT_CONFIG(tooltip)
        self.arbol_button.setToolTip("")
#endif // QT_CONFIG(tooltip)
        self.arbol_button.setText(QCoreApplication.translate("MainWindow", u"\u00c1rbol", None))
#if QT_CONFIG(tooltip)
        self.lista_button.setToolTip("")
#endif // QT_CONFIG(tooltip)
        self.lista_button.setText(QCoreApplication.translate("MainWindow", u"Lista", None))
        self.status.setText("")
        self.label.setText("")
        self.menuExtraer.setTitle(QCoreApplication.translate("MainWindow", u"Extraer", None))
    # retranslateUi

